/*
 * ----------------------------------------------------------------------------------
 *  ESP-12E Pinout:
 * ----------------------------------------------------------------------------------
 *                    ------------------------
 *                  --| Reset       D1 (TX0) |-- Serial TX/Prog RX
 *                  --| ADC         D3 (RX0) |-- Serial RX/Prog TX
 *              VCC --| CHPD        D4 (SCL) |-- TMP007 SCL
 *    Bold Brew Btn --| D16         D5 (SDA) |-- TMP007 SDA
 * Regular Brew Btn --| D14 (SCK)         D0 |-- Bootloader (low - program, high - normal)
 *       Status LED --| D12 (MISO)  D2 (TX1) |--
 *      Top Switch  --| D13 (MOSI)  D15 (SS) |-- GND (for normal startup)
 *                  --| VCC              GND |--
 *                    ------------------------
 * ----------------------------------------------------------------------------------
 */

#include <Arduino.h>
#include <ESP8266WiFi.h>
#include <ESP8266mDNS.h>
#include <ESP8266WebServer.h>
#include <ESP8266HTTPUpdateServerSPIFFS.h>
#include <WiFiManager.h>
#include <DNSServer.h>
#include <WiFiClient.h>
#include <PubSubClient.h>
#include <Wire.h>
#include <Adafruit_TMP007.h>
#include <EEPROM.h>
#include <FS.h>

#define TOP_SWITCH_PIN            13
#define STATUS_LED_PIN            12
#define REGULAR_BTN_PIN           16
#define BOLD_BTN_PIN              14

#define TEMP_UPDATE_INTERVAL      5000 // ms
#define TOO_COLD_TEMP             40   // °C

#define MQTT_STATE_TOPIC          "iotea/state"
#define MQTT_TEMP_TOPIC           "iotea/temperature"
#define MQTT_COMMAND_TOPIC        "iotea/command"

#define SETTINGS_REV              0xA1
#define MAX_STRING_LENGTH         128

enum EEPROMSettings
{
  SETTING_INITIALIZED,
  SETTING_READY_TO_DRINK_TEMP,
  SETTING_TEMP_OFFSET,
  SETTING_DELIM,
  // Max 128 characters for EEPROM stored strings below...
  SETTING_MDNS_LENGTH,
  SETTING_MDNS,
  SETTING_MQTT_SERVER_LENGTH = SETTING_MDNS + MAX_STRING_LENGTH,
  SETTING_MQTT_SERVER,
  SETTING_MQTT_USER_LENGTH = SETTING_MQTT_SERVER + MAX_STRING_LENGTH,
  SETTING_MQTT_USER,
  SETTING_MQTT_PASSWORD_LENGTH = SETTING_MQTT_USER + MAX_STRING_LENGTH,
  SETTING_MQTT_PASSWORD,
  NUM_OF_SETTINGS = SETTING_MQTT_PASSWORD + MAX_STRING_LENGTH
};

WiFiManager wifiManager;
MDNSResponder mdns;
ESP8266WebServer server(80);
ESP8266HTTPUpdateServer httpUpdater;

WiFiClient mqttClient;
PubSubClient mqtt(mqttClient);

Adafruit_TMP007 tmp007;

enum { IDLE, BREWING, BREW_CHECK, TEMP_WAIT, DONE, NOT_READY, NUM_STATES };
String stateNames[NUM_STATES] = { "Ready", "Brewing", "Brewing", "Waiting to Cool", "Ready to Drink!", "Not Ready" };
uint8_t state = IDLE;

enum { TOP_OPEN, NO_WATER, NOT_LOADED, READY, NUM_READY_STATES };
String readyStateNames[NUM_READY_STATES] = { "Top Open", "No Water", "Not Loaded", "" };
uint8_t readyState = READY;

String mdnsName;
uint8_t readyToDrinkTemp;
int8_t tempOffset;
String mqttServer;
String mqttUser;
String mqttPassword;

float mugTemp = 0;
float oldMugTemp = 0;
uint32_t stateTimer = 0;
bool mqttUpdate = true;

void setup()
{
  // Init GPIO
  pinMode(TOP_SWITCH_PIN, INPUT);
  pinMode(STATUS_LED_PIN, INPUT);
  pinMode(REGULAR_BTN_PIN, OUTPUT);
  pinMode(BOLD_BTN_PIN, OUTPUT);
  digitalWrite(REGULAR_BTN_PIN, LOW);
  digitalWrite(BOLD_BTN_PIN, LOW);

  // Initialize TMP007 (4 seconds per reading, 16 samples)
  tmp007.begin();

  // Initialize and read EEPROM settings
  EEPROM.begin(NUM_OF_SETTINGS);
  readEEPROMSettings();

  // Initialize SPIFFS
  SPIFFS.begin();

  // Connect to Wifi
  if (!wifiManager.autoConnect("Internet of Tea")) ESP.reset();

  // Start servers
  mdns.begin(mdnsName.c_str());
  server.on("/settings", HTTP_GET, []() { handleFileRead("/config.htm"); });
  server.on("/settings", HTTP_POST, saveSettings);
  server.on("/getsettings", getSettings);
  server.on("/reset", [&wifiManager]()
  {
    wifiManager.resetSettings();
    EEPROM.write(SETTING_INITIALIZED, 0);
    EEPROM.commit();
    server.send(200, "text/html", F("Clearing all saved settings (including Wifi) and restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.on("/restart", []()
  {
    server.send(200, "text/html", F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Restarting..."));
    delay(3000);
    ESP.restart();
  });
  server.onNotFound([]()
  {
    if (!handleFileRead(server.uri())) server.send(404, "text/plain", "Not Found");
  });
  httpUpdater.setup(&server);
  server.begin();

  mqtt.setServer(mqttServer.c_str(), 1883);
  mqtt.setCallback(mqttCallback);
  mqttReconnect();
}

void loop()
{
  server.handleClient();

  // If top opens at any time, we're not ready anymore
  if (readyState != TOP_OPEN && digitalRead(TOP_SWITCH_PIN))
  {
    state = NOT_READY;
    readyState = TOP_OPEN;
    mqttUpdate = true;
  }
  // If top just closed, we're ready again!
  else if (readyState == TOP_OPEN && !digitalRead(TOP_SWITCH_PIN))
  {
    state = IDLE;
    readyState = READY;
    mqttUpdate = true;
  }

  switch(state)
  {
    case IDLE:
      if (digitalRead(STATUS_LED_PIN))
      {
        stateTimer = millis();
        state = BREWING;
        mqttUpdate = true;
      }
    break;

    case BREWING:
      // When LED turns off, we need to check brew was successful
      if (!digitalRead(STATUS_LED_PIN))
      {
        // If LED turned off in under 30 seconds, not enough or no water
        if (millis() - stateTimer < 30000)
        {
          state = NOT_READY;
          readyState = NO_WATER;
          mqttUpdate = true;
        }
        else
        {
          stateTimer = millis();
          state = BREW_CHECK;
        }
      }
    break;

    case BREW_CHECK:
      // If LED turns on again, not enough or no water
      if (digitalRead(STATUS_LED_PIN))
      {
          state = NOT_READY;
          readyState = NO_WATER;
          mqttUpdate = true;
      }
      // If the lED has been off for more than 3 sec, we're done!
      else if (millis() - stateTimer > 3000)
      {
        stateTimer = 0;
        oldMugTemp = 0;
        state = TEMP_WAIT;
        mqttUpdate = true;
      }
    break;

    case TEMP_WAIT:
      // Wait for mug temperature to reach ready to drink temperature
      if (millis() - stateTimer > TEMP_UPDATE_INTERVAL)
      {
        char mugTempStr[7];
        mugTemp = tmp007.readObjTempC() + tempOffset;
        dtostrf(mugTemp, 5, 1, mugTempStr);
        mqtt.publish(MQTT_TEMP_TOPIC, mugTempStr);
        stateTimer = millis();

        // If temperature drops by more than 1.5 degrees, mug was taken away!
        // This bypasses DONE state, so no notification is triggered
        if ((oldMugTemp - mugTemp) > 1.5)
        {
          state = NOT_READY;
          readyState = NOT_LOADED;
          mqtt.publish(MQTT_TEMP_TOPIC, "0", true);
          mqttUpdate = true;
        }
        // Temperature rises initially, so only look for ready to drink temperature if falling
        else if (mugTemp < oldMugTemp && mugTemp <= readyToDrinkTemp)
        {
          state = DONE;
          mqttUpdate = true;
        }
        oldMugTemp = mugTemp;
      }
    break;

    case DONE:
      // If mug temperature drops below specified temperature, mug taken or got too cold
      if (millis() - stateTimer > TEMP_UPDATE_INTERVAL)
      {
        char mugTempStr[7];
        mugTemp = tmp007.readObjTempC() + tempOffset;
        dtostrf(mugTemp, 5, 1, mugTempStr);
        mqtt.publish(MQTT_TEMP_TOPIC, mugTempStr);
        stateTimer = millis();

        if (mugTemp <= TOO_COLD_TEMP)
        {
          state = NOT_READY;
          readyState = NOT_LOADED;
          mqtt.publish(MQTT_TEMP_TOPIC, "0", true);
          mqttUpdate = true;
        }
      }
    break;

    case NOT_READY:
      // Do nothing here
      // User will have to open the top to fix issue, which will be taken care of by code above
    break;

    default:
      state = IDLE;
    break;
  }

  if (!mqtt.connected()) mqttReconnect();

  if (mqtt.connected())
  {
    if (mqttUpdate)
    {
      String message = stateNames[state];
      if (state == NOT_READY) message += " - " + readyStateNames[readyState];
      mqtt.publish(MQTT_STATE_TOPIC, message.c_str(), true);
      mqttUpdate = false;
    }
    mqtt.loop();
  }
}

void pushButton(int pin)
{
  if (state == NOT_READY) return;

  digitalWrite(pin, HIGH);
  delay(100);
  digitalWrite(pin, LOW);
  delay(1000);

  stateTimer = millis();
  state = BREWING;
  mqttUpdate = true;
}

void initEEPROMSettings()
{
  EEPROM.write(SETTING_INITIALIZED, SETTINGS_REV);  // EEPROM initialized
  EEPROM.write(SETTING_READY_TO_DRINK_TEMP,   48);  // Ready to drink temperature (°C)
  EEPROM.write(SETTING_TEMP_OFFSET,            0);  // Temperature sensor offset (°C)
  EEPROM.write(SETTING_DELIM,                  0);  // Dummy setting to delimit start of strings
  writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, "iotea");
  writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, "");
  writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, "");
  EEPROM.commit();
}

void readEEPROMSettings()
{
  if (EEPROM.read(SETTING_INITIALIZED) != SETTINGS_REV) initEEPROMSettings();

  readyToDrinkTemp = EEPROM.read(SETTING_READY_TO_DRINK_TEMP);
  tempOffset = EEPROM.read(SETTING_TEMP_OFFSET);
  mdnsName = readEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH);
  mqttServer = readEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH);
  mqttUser = readEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH);
  mqttPassword = readEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH);
}

String readEEPROMString(uint16_t startAddress, uint16_t lengthAddress)
{
  String returnString = "";
  uint8_t length = constrain(EEPROM.read(lengthAddress), 0, MAX_STRING_LENGTH);
  for (int i = 0; i < length; i++) returnString += (char)EEPROM.read(startAddress + i);
  return returnString;
}

void writeEEPROMString(uint16_t startAddress, uint16_t lengthAddress, String str)
{
  uint8_t length = constrain(str.length(), 0, MAX_STRING_LENGTH);
  EEPROM.write(lengthAddress, length);
  for (int i = 0; i < length; i++) EEPROM.write(startAddress + i, str[i]);
  EEPROM.commit();
}

void getSettings()
{
  String response;
  IPAddress ip = WiFi.localIP();

  response = mdnsName;
  response += ",";
  response += mqtt.connected();
  response += ",";
  response += String(ip[0]) + "." + String(ip[1]) + "." + String(ip[2]) + "." + String(ip[3]);
  response += ",";
  response += mqttServer;
  response += ",";
  response += mqttUser;
  response += ",";
  response += mqttPassword;
  response += ",";

  for (uint8_t i = 1; i < SETTING_DELIM; i++)
  {
    response += EEPROM.read(i);
    response += ",";
  }

  server.sendHeader("Cache-Control", "no-cache");
  server.send(200, "text/plain", response);
}

void saveSettings()
{
  String response;

  if (server.hasArg("readyToDrinkTemp"))
  {
    readyToDrinkTemp = server.arg("readyToDrinkTemp").toInt();
    EEPROM.write(SETTING_READY_TO_DRINK_TEMP, readyToDrinkTemp);
  }
  if (server.hasArg("tempOffset"))
  {
    tempOffset = server.arg("tempOffset").toInt();
    EEPROM.write(SETTING_TEMP_OFFSET, tempOffset);
  }
  if (server.hasArg("mdnsName"))
  {
    mdnsName = server.arg("mdnsName");
    writeEEPROMString(SETTING_MDNS, SETTING_MDNS_LENGTH, mdnsName);
  }
  if (server.hasArg("mqttServer"))
  {
    mqttServer = server.arg("mqttServer");
    writeEEPROMString(SETTING_MQTT_SERVER, SETTING_MQTT_SERVER_LENGTH, mqttServer);
  }
  if (server.hasArg("mqttUser"))
  {
    mqttUser = server.arg("mqttUser");
    writeEEPROMString(SETTING_MQTT_USER, SETTING_MQTT_USER_LENGTH, mqttUser);
  }
  if (server.hasArg("mqttPassword"))
  {
    mqttPassword = server.arg("mqttPassword");
    writeEEPROMString(SETTING_MQTT_PASSWORD, SETTING_MQTT_PASSWORD_LENGTH, mqttPassword);
  }
  EEPROM.commit();

  response = F("<META http-equiv=\"refresh\" content=\"10;URL='/'\">Settings saved! Restarting to take effect...<br><br>");
  server.send(200, "text/html", response);
  delay(1000);
  ESP.reset();
}

bool handleFileRead(String path)
{
  if (path.endsWith("/")) path += "index.htm";

  String dataType = "text/plain";
  if (path.endsWith(".htm")) dataType = "text/html";
  else if (path.endsWith(".css")) dataType = "text/css";

  if (SPIFFS.exists(path))
  {
    File file = SPIFFS.open(path, "r");
    server.streamFile(file, dataType);
    file.close();
    return true;
  }
  return false;
}

void mqttReconnect()
{
  static long lastReconnect = 0;

  if (millis() - lastReconnect > 5000)
  {
    if (mqtt.connect("Internet of Tea", mqttUser.c_str(), mqttPassword.c_str()))
    {
      mqtt.subscribe(MQTT_COMMAND_TOPIC);
      lastReconnect = 0;
    }
    else lastReconnect = millis();
  }
}

void mqttCallback(char* topic, byte* payload, unsigned int length)
{
  if (!strncasecmp_P((char*)payload, "brew", length)) { pushButton(REGULAR_BTN_PIN); }
  else if (!strncasecmp_P((char*)payload, "brew bold", length)) { pushButton(BOLD_BTN_PIN); }
  else if (!strncasecmp_P((char*)payload, "cancel brew", length)) { pushButton(REGULAR_BTN_PIN); state = IDLE; mqttUpdate = true; }
}
