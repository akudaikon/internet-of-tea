var state = document.getElementById('state'),
    readyState = document.getElementById('readyState'),
    stateIcon = document.getElementById('stateIcon'),
    brewBtn = document.getElementById('brewBtn'),
    boldBtn = document.getElementById('boldBtn'),
    brewDiv = document.getElementById('brewDiv'),
    brewCancelBtn = document.getElementById('brewCancelBtn'),
    brewCancelDiv = document.getElementById('brewCancelDiv'),
    ip = document.getElementById('ip'),
    oldTemp = 0.0;

var mqttOptions =
{
  keepalive: 10,
  clientId: 'Browser-' + Math.random().toString(16).substr(2, 8),
  protocolId: 'MQTT',
  protocolVersion: 4,
  clean: true,
  reconnectPeriod: 5000,
  connectTimeout: 30 * 1000,
  rejectUnauthorized: false
}

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        ip.innerHTML = "<small>MQTT " + (settings[1] > 0 ? "Connected" : "Disconnected") + "<br>" + settings[0] + ".local<br>IP Address: " + settings[2] + "</small>";

        mqttOptions['username'] = settings[4];
        mqttOptions['password'] = settings[5];
        var client = mqtt.connect('ws://' + settings[3] + ':1884', mqttOptions);

        client.subscribe("iotea/temperature");
        client.subscribe("iotea/state");

        brewBtn.onclick = function()
        {
          client.publish('iotea/command', 'brew');
          brewBtn.disabled = true;
          boldBtn.disabled = true;
        }

        boldBtn.onclick = function()
        {
          client.publish('iotea/command', 'brew bold');
          brewBtn.disabled = true;
          boldBtn.disabled = true;
        }

        brewCancelBtn.onclick = function()
        {
          client.publish('iotea/command', 'cancel brew');
        }

        client.on("message", function (topic, payload)
        {
          console.log(topic);
          console.log(payload.toString());

          if (topic == "iotea/state")
          {
            var states = payload.toString().split("-");
            var curState = states[0].trim();
            state.innerHTML = curState;

            if (curState == "Ready")
            {
              stateIcon.innerHTML = "<i class='fa fa-check' style='color:yellowgreen'></i>";
              readyState.style.display = "none";
              brewDiv.style.display = "block";
              brewBtn.disabled = false;
              boldBtn.disabled = false;
              brewCancelDiv.style.display = "none";
            }
            else if (curState == "Brewing")
            {
              stateIcon.innerHTML = "<i class='fa fa-spin fa-spinner'></i>";
              readyState.style.display = "none";
              brewDiv.style.display = "none";
              brewCancelDiv.style.display = "block";
              oldTemp = 0.0;
            }
            else if (curState == "Waiting to Cool")
            {
              stateIcon.innerHTML = "<i class='fa fa-thermometer-half' style='color:tomato'></i>";
              readyState.style.display = "block";
              brewDiv.style.display = "none";
              brewCancelDiv.style.display = "none";
            }
            else if (curState == "Ready to Drink!")
            {
              stateIcon.innerHTML = "<i class='fa fa-thumbs-o-up' style='color:steelblue'></i>";
              readyState.style.display = "block";
              brewDiv.style.display = "none";
              brewCancelDiv.style.display = "none";
            }
            else if (curState == "Not Ready")
            {
              stateIcon.innerHTML = "<i class='fa fa-warning' style='color:red'></i>";
              readyState.style.display = "block";
              readyState.innerHTML = states[1].toString();
              brewDiv.style.display = "block";
              brewBtn.disabled = true;
              boldBtn.disabled = true;
              brewCancelDiv.style.display = "none";
            }
          }
          else if (topic == "iotea/temperature")
          {
            var curTemp = parseFloat(payload.toString());
            readyState.innerHTML = "Currently " + curTemp.toFixed(1) + "&deg;C ";
            if (curTemp > oldTemp) { readyState.innerHTML += "<span style='color:red'><i class='fa fa-angle-up'></i></span>"; }
            else { readyState.innerHTML += "<span style='color:blue'><i class='fa fa-angle-down'></i></span>"; }
            oldTemp = curTemp;
          }
        });
      }
    }
  };
  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);
