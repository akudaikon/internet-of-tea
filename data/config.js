var readyToDrinkTemp = document.getElementById('readyToDrinkTemp'),
    tempOffset = document.getElementById('tempOffset'),
    mdnsName = document.getElementById('mdnsName'),
    mqttServer = document.getElementById('mqttServer'),
    mqttUser = document.getElementById('mqttUser'),
    mqttPassword = document.getElementById('mqttPassword');

getSettings = function()
{
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (xhttp.readyState == 4) {
      if (xhttp.status == 200) {
        var settings = xhttp.responseText.split(",");
        mdnsName.value = settings[0];
        mqttServer.value = settings[3];
        mqttUser.value = settings[4];
        mqttPassword.value = settings[5];
        readyToDrinkTemp.value = settings[6];
        tempOffset.value = settings[7];
      }
    }
  };
  xhttp.open("GET", "getsettings", true);
  xhttp.send();
}

// To get around onLoad not firing on back
setTimeout(getSettings, 100);